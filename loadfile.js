// function loadAllStudent(){
//     var responseData=null;
//     var data=fetch('https://dv-student-backend-2019.appspot.com/students')
//     .then((response)=>{
//         console.log(response)
//        return response.json()
//     }).then((json)=>{
//         responseData= json
//         var resultElement=document.getElementById('result')
//     resultElement.innerHTML=JSON.stringify(json,null,2);
//     return data
// })
//     }


async function loadAllStudentAsync() {
    let response = await fetch('https://dv-student-backend-2019.appspot.com/students')
    let data = await response.json();
    return data
}

function createResultTable(data) {
    let resultElement = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')


    //create the header
    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)

    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)
    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = '#'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'studentId'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'surname'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'gpa'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((json) => {
        for (let i = 0; i < json.length; i++) {
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)
            var dataFrirstColumnNode = document.createElement('th')
            dataFrirstColumnNode.setAttribute('scope', 'row')
            dataFrirstColumnNode.innerHTML = currentData['id']
            dataRow.appendChild(dataFrirstColumnNode)
    
            var columNode = null;
            columNode = document.createElement('td')
            columNode.innerText = currentData['studentId']
            dataRow.appendChild(columNode)
    
            columNode = document.createElement('td')
            columNode.innerText = currentData['name']
            dataRow.appendChild(columNode)
    
            columNode = document.createElement('td')
            columNode.innerText = currentData['surname']
            dataRow.appendChild(columNode)
    
            columNode = document.createElement('td')
            columNode.innerText = currentData['gpa']
            dataRow.appendChild(columNode)
    
            columNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['image'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)
    
    
        }
    })
}












function createResultTable2(data1) {
    let resultElement = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')


    //create the header
    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)

    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)

    
    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = '#'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'studentId'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'surname'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'gpa'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data1)

    data1.then((json) => {
        
            var currentData = json
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)
            var dataFrirstColumnNode = document.createElement('th')
            dataFrirstColumnNode.setAttribute('scope', 'row')
            dataFrirstColumnNode.innerHTML = currentData['id']
            dataRow.appendChild(dataFrirstColumnNode)
    
            var columNode = null;
            columNode = document.createElement('td')
            columNode.innerText = currentData['studentId']
            dataRow.appendChild(columNode)
    
            columNode = document.createElement('td')
            columNode.innerText = currentData['name']
            dataRow.appendChild(columNode)
    
            columNode = document.createElement('td')
            columNode.innerText = currentData['surname']
            dataRow.appendChild(columNode)
    
            columNode = document.createElement('td')
            columNode.innerText = currentData['gpa']
            dataRow.appendChild(columNode)
    
            columNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['image'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)
    
    
        }
    )
}

async function loadOneStudent() {
    let studentId = document.getElementById('queryId').value
    if (studentId != '' && studentId != null) {
        let response = await fetch('https://dv-student-backend-2019.appspot.com/students/'+studentId)
        let data2 = await response.json();
        return data2
    }
   

}


