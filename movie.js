async function loadAllStudentAsync() {
    let response = await fetch('https://dv-excercise-backend.appspot.com/movies')
    let data = await response.json(); 
    return data
}

function createResultTable(data) {
    let resultElement = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')


    //create the header
    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)

    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)
    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'synopsis'
    tableHeadNode.appendChild(tableHeaderNode)


    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

  

    data.then((json) => {
        for (let i = 0; i < json.length; i++) {
            var currentData = json[i]
        
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)
            var dataFrirstColumnNode = document.createElement('th')
            dataFrirstColumnNode.setAttribute('scope', 'row')
            dataFrirstColumnNode.innerHTML = currentData['name']
            dataRow.appendChild(dataFrirstColumnNode)


            var columNode = null;
            columNode = document.createElement('td')
            columNode.innerText = currentData['synopsis']
            dataRow.appendChild(columNode)
    
  
            columNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['imageUrl'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)
    
    
        }
    })
}

function createResultTable2(data1) {
    let checkResult=document.getElementById('queryId')

    let resultElement = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')


    //create the header
    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    
    //Set the row of the table
    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)

    //Set the Name column
    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'name'
    tableHeadNode.appendChild(tableHeaderNode)

    //Set the Synopsis column
    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'synopsis'
    tableHeadNode.appendChild(tableHeaderNode)

    //Set the Image column
    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data1)

    data1.then((json) => {
        for (let i = 0; i < json.length; i++) {
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)
            var dataFrirstColumnNode = document.createElement('th')
            dataFrirstColumnNode.setAttribute('scope', 'row')
            dataFrirstColumnNode.innerHTML = currentData['name']
            dataRow.appendChild(dataFrirstColumnNode)


            var columNode = null;
            columNode = document.createElement('td')
            columNode.innerText = currentData['synopsis']
            dataRow.appendChild(columNode)
    
  
            columNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['imageUrl'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)
    
    
        }
    })

}

async function loadOneStudent() {
    let name = document.getElementById('queryId').value
    if (name != '' && name != null) {
        let response = await fetch('https://dv-excercise-backend.appspot.com/movies/'+name)
        let data2 = await response.json();
        return data2
    }
}

function resetButton(clickedId, data2) {
    let resultElement = document.getElementById('resultTable')

    //Create the table
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')

    //create the header
    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    
    //Set the row of the table
    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)

    //Set the Name column
    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'name'
    tableHeadNode.appendChild(tableHeaderNode)

    //Set the Synopsis column
    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'synopsis'
    tableHeadNode.appendChild(tableHeaderNode)

    //Set the Image column
    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'image'
    tableHeadNode.appendChild(tableHeaderNode)

    if(resultElement != null) {
        resultElement.removeChild()
    }

    data2.then((json) => {
        var currentData = json
        
        var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)

        var dataFrirstColumnNode = document.createElement('th')
            dataFrirstColumnNode.setAttribute('scope', 'row')
            dataFrirstColumnNode.innerHTML = currentData['name']
            dataRow.appendChild(dataFrirstColumnNode)

        var columNode = null;
            columNode = document.createElement('td')
            columNode.innerText = currentData['synopsis']
            dataRow.appendChild(columNode)
    
            columNode = document.createElement('td')

        var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['imageUrl'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)
    })
}

// function resetResultTable2(data1) {
//     let resultElement = document.getElementById('resultTable')
//     let checkResult = document.getElementById('queryId')

//     if(checkResult != null) {
//         resultElement.removeChild(resultElement)
//     }
    
//     // if(elementResult != null){
//     //     tableHeadNode.removeChild(elementResult)
//     // }

//     data1.then((json) => {
//         for (let i = 0; i < json.length; i++) {
//             var currentData = json[i]
       
           
        
//             var dataRow = document.createElement('tr')
//             tableNode.appendChild(dataRow)
//             var dataFrirstColumnNode = document.createElement('th')
//             dataFrirstColumnNode.setAttribute('scope', 'row')
//             dataFrirstColumnNode.innerHTML = " "
//             dataRow.appendChild(dataFrirstColumnNode)


//     var columNode = null;
//             columNode = document.createElement('td')
//             columNode.innerText = " "
//             dataRow.appendChild(columNode)
    
  
//             columNode = document.createElement('td')
//             var imageNode = document.createElement('img')
//             imageNode.setAttribute('src', currentData[' '])
//             imageNode.style.width = '200px'
//             imageNode.style.height = '200px'
//             dataRow.appendChild(imageNode)
    
    
//         }
//     })
// }

// async function ResetRow() {
//     let name = document.getElementById('queryId').value
//     if (name != '' && name != null) {
//         let response = await fetch('https://dv-excercise-backend.appspot.com/movies/'+name)
//         let data2 = await response.json();
//         return data2;
//     }
   

// }

